-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2021 at 04:08 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inv_for_cdip`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch`
--

CREATE TABLE `tbl_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(20) NOT NULL,
  `branch_name` varchar(30) NOT NULL,
  `branch_name_bn` varchar(30) DEFAULT NULL,
  `area_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=Inactive, 1=Active',
  `branch_contact_no` varchar(15) DEFAULT NULL,
  `branch_address` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_branch`
--

INSERT INTO `tbl_branch` (`id`, `branch_code`, `branch_name`, `branch_name_bn`, `area_id`, `status`, `branch_contact_no`, `branch_address`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '001', 'Boali', NULL, 3, 1, NULL, NULL, '2021-11-17 23:52:36', NULL, NULL, NULL),
(2, '001', 'Boali', NULL, 3, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(3, '002', 'Ramchandrapur', NULL, 3, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(4, '0002', 'Gaibandha-2', NULL, 2, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(5, '003', 'Gaibandha Sadar', NULL, 1, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(6, '004', 'Kamarjani ', NULL, 5, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(7, '005', 'Fazlupur', NULL, 1, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(8, '006', 'Udakhali', NULL, 3, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(9, '007', 'Konchipara', NULL, 1, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(10, '008', 'Gidari', NULL, 1, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(11, '009', 'Dariapur', NULL, 1, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(12, '010', 'Chandipur', NULL, 4, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(13, '011', 'Tarapur', NULL, 4, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(14, '012', 'Belka', NULL, 4, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(15, '013', 'Arendabari', NULL, 5, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(16, '014', 'Gajaria', NULL, 3, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(17, '015', 'Sreepur', NULL, 4, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(18, '016', 'Badiakhali', NULL, 3, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(19, '017', 'Rajibpur', NULL, 5, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(20, '018', 'Saghata ', NULL, 3, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(21, '019', 'Laxmipur', NULL, 4, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(22, '020', 'Palashbari', NULL, 7, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(23, '021', 'Gobindaganj ', NULL, 6, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(24, '022', 'Rowmari', NULL, 5, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(25, '023', 'Shothibari', NULL, 7, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(26, '024', 'Rangpur Sadar ', NULL, 7, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(27, '025', 'Darshana', NULL, 7, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(28, '026', 'Kortimari', NULL, 5, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(29, '027', 'CO Bazar', NULL, 7, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(30, '028', 'Nilphamari Sadar', NULL, 8, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(31, '029', 'Kazirhat', NULL, 8, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(32, '030', 'Raniganj', NULL, 6, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(33, '031', 'Birampur', NULL, 11, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(34, '032', 'Fulbari', NULL, 11, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(35, '033', 'Fashitola', NULL, 6, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(36, '034', 'Jaigirhat', NULL, 7, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(37, '035', 'Bhaduria', NULL, 12, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(38, '036', 'Shibganj', NULL, 6, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(39, '037', 'Joypurhat Sadar ', NULL, 12, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(40, '038', 'Dinajpur Sadar', NULL, 11, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(41, '039', 'Saidpur', NULL, 8, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(42, '040', 'Lalmonirhat Sadar', NULL, 10, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(43, '041', 'Hakimpur', NULL, 12, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(44, '042', 'Kurigram Sadar', NULL, 10, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(45, '043', 'Domar', NULL, 8, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(46, '044', 'Birol', NULL, 11, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(47, '045', 'Chirirbandar', NULL, 8, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(48, '046', 'Pulhat', NULL, 11, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(49, '047', 'Rajarhat', NULL, 10, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(50, '048', 'Aditmari', NULL, 10, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(51, '049', 'Panchbibi', NULL, 12, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(52, '050', 'Akkelpur', NULL, 12, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(53, '051', 'Jamalganj', NULL, 12, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(54, '052', 'Bogura Sadar', NULL, 6, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(55, '053', 'Pollimangal', NULL, 6, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL),
(56, '0009', 'Birampur Area', NULL, 9, 1, NULL, NULL, '2021-11-17 23:59:46', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_code` (`area_id`),
  ADD KEY `branch_code` (`branch_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  ADD CONSTRAINT `tbl_branch_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `tbl_area` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
