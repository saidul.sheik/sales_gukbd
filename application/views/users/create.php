

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage
        <small>Users</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12 col-xs-12">
          
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-error alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
          <?php endif; ?>

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add User</h3>
            </div>
            <form role="form" action="<?php base_url('users/create') ?>" method="post">
              <div class="box-body">
                <?php echo validation_errors(); ?>
				
                <?php 
                 /* $branchArray = array();
                  $areaArray = array();
                  $zoneArray = array();

                  foreach ($zoneareabranch as $key => $value) {
                    $branchArray[]=array(
                      'branch_id'   =>$value->branch_id,
                      'branch_code'   =>$value->branch_code,
                      'branch_name'   =>$value->branch_name,
                    ); 
                    $areaArray[]=array(
                      'area_id'     =>$value->area_id,
                      'area_code'   =>$value->area_code,
                      'area_name'   =>$value->area_name,
                      'zone_id'     =>$value->zone_id,
                    );

                    $zoneArray[]=array(
                      'zone_id'     =>$value->zone_id,
                      'zone_code'   =>$value->zone_code,
                      'zone_name'   =>$value->zone_name,
                    );
                  }

                 $areas = array_unique($areaArray, SORT_REGULAR);
                 $zones = array_unique($zoneArray, SORT_REGULAR);


                  echo "<pre>";
                  print_r($zones);
                  echo "</pre>";  

                  echo "<pre>";
                  print_r($areas);
                  echo "</pre>"; */
                 /* foreach ($zones as $key => $value) {
                     echo "<pre>";
                    print_r($value->id);
                    echo "</pre>";
                  }*/
                 

                ?>


				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="form-group">
						  <label for="groups">User Role</label>
						  <select class="form-control select_2" id="groups" name="groups">
							<option value="">Select Role</option>
							<?php foreach ($group_data as $k => $v): ?>
							  <option value="<?php echo $v['id'] ?>"><?php echo $v['group_name'] ?></option>
                <option value="<?php echo $v['id'] ?>" <?php echo set_select('groups', $v['id']);?> ><?php echo $v['group_name'] ?></option>
							<?php endforeach; ?>
						  </select>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="form-group">
						  <label for="zone_id">Select Zone</label>
						  <select class="form-control select_2" id="zone_id" name="zone_id">
							<option value="">Select Zone</option>
							<?php foreach ($zones as $key => $value): ?>
							  <option value="<?php echo $value->id; ?>"  <?php echo set_select('zone_id', $value->id);?> ><?php echo $value->zone_name;  ?></option>
							<?php endforeach; ?>
						  </select>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="form-group">
						  <label for="area_id">Select Area</label>
						  <select class="form-control select_2" id="area_id" name="area_id">
							 <option value="">Select Area</option>
						  </select>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4">
						<div class="form-group">
						  <label for="branch_code">Select Branch</label>
						  <select class="form-control select_2" id="branch_id" name="branch_id">
							<option value="">Select Branch</option>
							
						  </select>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="form-group">
						  <label for="username">Username</label>
						  <input type="text" class="form-control" id="username" name="username" placeholder="Username" autocomplete="off">
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="form-group">
						  <label for="email">Email</label>
						  <input type="email" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off">
						</div>
					</div>
					
					
				</div>

                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="text" class="form-control" id="password" name="password" placeholder="Password" autocomplete="off">
                </div>

                <div class="form-group">
                  <label for="cpassword">Confirm password</label>
                  <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" autocomplete="off">
                </div>

                <div class="form-group">
                  <label for="fname">First name</label>
                  <input type="text" class="form-control" id="fname" name="fname" placeholder="First name" autocomplete="off">
                </div>

                <div class="form-group">
                  <label for="lname">Last name</label>
                  <input type="text" class="form-control" id="lname" name="lname" placeholder="Last name" autocomplete="off">
                </div>

                <div class="form-group">
                  <label for="phone">Phone</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" autocomplete="off">
                </div>

                <div class="form-group">
                  <label for="gender">Gender</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="gender" id="male" value="1">
                      Male
                    </label>
                    <label>
                      <input type="radio" name="gender" id="female" value="2">
                      Female
                    </label>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <a href="<?php echo base_url('users/') ?>" class="btn btn-warning">Back</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $(".select_2").select2();
    $("#mainUserNav").addClass('active');
    $("#createUserNav").addClass('active');
    

   /* $("#zone_id").change(function(){
        var zone = $(this).val();
    });*/

  });

  /*ger area by zone*/
  $(document).on("change", "#zone_id", function () {
    var zone_id = $(this).val();  
     $("#branch_id").html("");
    $.ajax({
      url : "getAreaByZoneID"+"/"+zone_id,
      type: "GET",
      dataType: 'json',
      success: function(data)
      {
         $("#branch_id").html("<option value=''>-Select Branch -</option>"); 
        var options = "<option value=''>-Select Area-</option>"; 
            for(var x in data) { 
             console.log(data[x]["id"]);
              options += "<option   <?php echo set_select('area_id', $value->id); ?> value="+data[x]["id"]+"  >"+data[x]["area_name"]+"</option>";   
             
             }  
          $('#area_id').html(options);
      }
    });  
  });

   /*ger branch by area*/
  $(document).on("change", "#area_id", function () {
    var area_id = $(this).val();  
    $.ajax({
      url : "getBranchByAreaID"+"/"+area_id,
      type: "GET",
      dataType: 'json',
      success: function(data)
      {
         
        var options = "<option value=''>-Select Branch-</option>"; 
            for(var x in data) { 
             console.log(data[x]["id"]);
              options += "<option <?php echo set_select('branch_id', $value->id); ?>  value="+data[x]["id"]+"  >"+data[x]["branch_name"]+"</option>";   
             
             }  
          $('#branch_id').html(options);
      }
    });  
  });


</script>
