-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2021 at 04:08 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inv_for_cdip`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_area`
--

CREATE TABLE `tbl_area` (
  `id` int(11) NOT NULL,
  `area_code` varchar(20) NOT NULL,
  `area_name` varchar(30) NOT NULL,
  `area_name_bn` varchar(30) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  `area_contact_no` varchar(15) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_area`
--

INSERT INTO `tbl_area` (`id`, `area_code`, `area_name`, `area_name_bn`, `zone_id`, `status`, `area_contact_no`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'A-01', 'Gaibandha-1/Dariapur', '', 1, 1, NULL, '2021-11-17 23:48:35', NULL, NULL, NULL),
(2, 'A-02', 'Gaibandha-2/Boali', '', 1, 1, NULL, '2021-11-17 23:48:54', NULL, NULL, NULL),
(3, 'A-03', 'Badiakhali', '', 1, 1, NULL, '2021-11-17 23:49:08', NULL, NULL, NULL),
(4, 'A-04', 'Sundorgonj Area', '', 1, 1, NULL, '2021-11-17 23:49:08', NULL, NULL, NULL),
(5, 'A-05', 'Rajibpur Area', '', 1, 1, NULL, '2021-11-17 23:49:08', NULL, NULL, NULL),
(6, 'A-06', 'Gobindogonj Area', '', 1, 1, NULL, '2021-11-17 23:49:08', NULL, NULL, NULL),
(7, 'A-07', 'Rangpur Area', '', 2, 1, NULL, '2021-11-17 23:50:27', NULL, NULL, NULL),
(8, 'A-08', 'Nilphamari Area', '', 2, 1, NULL, '2021-11-17 23:50:27', NULL, NULL, NULL),
(9, 'A-09', 'Birampur Area', '', 2, 1, NULL, '2021-11-17 23:50:27', NULL, NULL, NULL),
(10, 'A-10', 'Lalmonirhat Area', '', 2, 1, NULL, '2021-11-17 23:50:27', NULL, NULL, NULL),
(11, 'A-11', 'Dinajpur Area', '', 2, 1, NULL, '2021-11-17 23:50:27', NULL, NULL, NULL),
(12, 'A-12', 'Joypurhat Area', '', 2, 1, NULL, '2021-11-17 23:50:27', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_area`
--
ALTER TABLE `tbl_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zone_id` (`zone_id`),
  ADD KEY `zone_id_2` (`zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_area`
--
ALTER TABLE `tbl_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_area`
--
ALTER TABLE `tbl_area`
  ADD CONSTRAINT `tbl_area_ibfk_1` FOREIGN KEY (`zone_id`) REFERENCES `tbl_zone` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
